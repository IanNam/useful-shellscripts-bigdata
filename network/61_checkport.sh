#!/bin/sh

echo "Enter IP Address: "
read ipaddr
echo "Start '$ipaddr' 80 2222 8080 port is open..."

faillog="./text/fail-port.log"
for port in 80 2222 8080
do
    nc -w 5 -z $ipaddr $port

    if [ $? -ne 0 ]; then
        echo "Failed at port: $port" >> "$faillog"
    fi
done

