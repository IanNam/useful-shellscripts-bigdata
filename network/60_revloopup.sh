#!/bin/sh

if [ "$1" == "-h" ]; then
    echo "Get hostname from given ip address."
    echo "Usage: $0 *.txt [text file, ip address listed.]"
    exit 0
fi

while read ipaddr
do 
    #host -> ipaddr
    revlookup=$(host "$ipaddr")

    #check host command is suceeded or not
    if [ $? -eq 0 ]; then
        echo -n $"ipaddr,"
        echo "$revlookup" | awk '{print $NF}' | sed 's/\.$//'
    else
        echo "$ipaddr,"
    fi

    sleep 1
done < $1
