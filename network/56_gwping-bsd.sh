if [ "$1" == "-h" ]; then
    echo "[Linux] Check network connection to default gateway from ping command."
    echo "Usage: 'basename $0'"
    exit 0
fi

gateway=$(netstat -nr | awk '$1 == "default" {print $2}')
  
ping -c 1 $gateway > /dev/null 2>&1

if [ $? -eq 0 ]; then
  echo "[Sucess] ping -> $gateway"
else
  echo "[Failed] ping -> $gateway"
fi