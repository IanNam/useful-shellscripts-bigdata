# Get the Average Response Time from remote server as ping command.
if [ "$1" == "-h" ]; then
    echo "Get the Average Response Time from remote server as ping command"
    echo "Usage: 'basename $0'"
    exit 0
fi

echo "Enter Remote IP Address: "
read id

echo "Remote IP Address is $id"

echo "Enter Ping Count Number"
read nums

ipaddr=$id
count=$nums

echo "ping to: $ipaddr"
echo "ping count: $count"
echo "ping average[ms]:"

ping -c $count $ipaddr > ping.$$

sed -n "s/^.*time=\(.*\) ms/\1/p" ping.$$ |\
awk '{sum+=$1} END{print sum/NR}'

rm -f ping.$$
